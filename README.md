# Data export from OCI to DC using DTA


## Introduction

Appliance-based Data Export is Oracle's offline data export solution that lets you migrate petabyte-scale datasets from OCI Object Storage bucket to data center using an Oracle-provided Data Transfer Appliance.


## Data Transfer Appliance - export

The Data Transfer Appliance (export appliance) is a high-storage capacity device used to export data from Oracle Cloud Infrastructure to your data center. In general, you request an export appliance from Oracle, specify the data files to be copied from your Object Storage bucket to the export appliance, and then have the export appliance containing the data shipped to you. After copying data you are responsible for deleting and sending export appliance back to Oracle. You can find detailed appliance specification [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/export_overview.htm). Take into the consideration the following:
-	Data Export is not available for OCI free trial or Pay As You Go accounts
-	Service availability depends on the region – find supported regions list [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/supported_regions.htm)
- Data Transfer appliance availability is based on inventory per region. Oracle distributes appliances on a first come, first serve basis based on customer request
- Appliance storage capacity: 50TB of protected usable space (depends on Supported OCI Region)

When it comes to the security aspects, see the following:  
- Data at rest is encrypted with AES-256 encryption
- Node-to-node communication is encrypted with GCM-AES-128
- Console and API are using TLS and will default to AES-256
- Appliances are shipped to you with a tamper-evident security tie on the transit case.

## How Data export works - Data Transfer Appliance

As described above, this solution supports data transfer when you are migrating a large volume of data and when using a transfer disk is not a practical alternative. You simply export your data from your OCI Object Storage bucket to your data center using an Oracle-provided appliance.
The whole data export process for DTA is summarized below. Please note that some steps require to use of the OCI CLI - not everything can be done via the OCI Console (below you see what is required: CLI, Console, or can be both).
1. Request entitlement - **OCI CLI** or **OCI Console**
2. E-sign Terms & Conditions 
3. Install CLI on Linux host
4. Setup notifications
5. Create an Export Job
6. Specify (standard) bucket and shipping details
7. Create a manifest in bucket
8. Establish IAM policies (add provided policy language to tenancy)
9. Request the Export Appliance
10. Oracle encrypts appliance, downloads data to it before shipping
11. Monitor data transfer progress - **OCI CLI** or **OCI Console**
12. Receive appliance
13. Connect appliance to the network
14. Connect to the appliance via serial console and assign IP
15. Unlock appliance, mount NFS dataset and copy data
16. Review the download summary on Appliance against manifest
17. Erase data and ship appliance back to Oracle for NIST SP 800-88 wipe
 
Detailed steps description can be found [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Tasks/export_preparing.htm)

Notes:
-	you cannot export from Archive bucket directly. You first must restore to a regular tier Object bucket and then specify that bucket name
-	The Manifest is the list of files you intend to have Oracle export from your bucket to the appliance and then shipped to you. It is stored in your bucket. You can use prefixes and start/end to help with the selection process. Up to 150TB per export 
-	Oracle uses the manifest to download from your bucket all the files that are listed in the manifest. When the DTA is shipped back to you, the download summary is available at the root of the DTA mount point so you may compare the manifest against the download summary
-	What was actually downloaded is on the DTA itself so the user can cross check manifest with the data if you want to (you have to do “file compare” - a download summary is available at the root of the DTA mount point).

